FROM justcontainers/base
MAINTAINER Michael Katica <michael.katica@videa.tv>

RUN add-apt-repository -y "deb http://archive.ubuntu.com/ubuntu trusty-backports main restricted universe multiverse"
RUN add-apt-repository -y "deb http://archive.ubuntu.com/ubuntu trusty main restricted universe multiverse"
RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -t trusty-backports -y curl

RUN cd /tmp/ && \
    curl -O https://dl.influxdata.com/chronograf/releases/chronograf_1.0.0_amd64.deb && \
    dpkg -i chronograf_1.0.0_amd64.deb

ADD chronograf/services.d/chronograf /etc/services.d/10-chronograf/run
RUN chmod -R +x /etc/services.d/ 

EXPOSE 10000
ENTRYPOINT ["/init"]
